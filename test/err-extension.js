'use strict'

const extension = require('../lib/extension')

module.exports.register = (registry, context = null, filesource = null) =>
  extension.doRegister(registry, 'sample', (lines) => {throw new Error('Error!')}, context, filesource)
